 $( function() {
 
    // Let the cards and functions items be draggable
    $( ".bbgCard, .bbgFunction" ).draggable({
      revert: "invalid", // when not dropped, the item will revert back to its initial position
      containment: "document",
      helper: "clone",
      cursor: "move"
    });
	
	// Let the comand line be droppable, accepting the draggable items
    $( ".slotEmpty" ).droppable({
      accept: ".bbgCard, .bbgFunction",
      classes: {
        "ui-droppable-active": "ui-state-highlight"
      },
      drop: function( event, ui ) {
		var $cardType = $(ui.draggable).attr("data-cardType");
		var $cardValue = $(ui.draggable).attr("data-cardValue");
		if($cardType == 'hint' | $cardType == 'function')  {
			$("#comandLine .slot").empty().html("EMPTY");
		}
        $(this).empty().removeClass("slotEmpty").append($(ui.draggable).clone());
		var $cardType = $(ui.draggable).attr("data-cardType");
		var $cardValue = $(ui.draggable).attr("data-cardValue");
		$("#answerBoard").html($cardType+":"+$cardValue);
      }
    });

 });
 
$(document).on('click', '.enterBtn', function() {
	var $argTypes = $("#comandLine .slot:not(.enterBtn) .bbgCard, #comandLine .slot:not(.enterBtn) .bbgFunction").attr("data-cardType");
/*
	$(".slot:not(.enterBtn)").each(function(i, obj) {
		if ($(obj).attr('data-cardType').length) {
			$argTypes[i] = (obj).attr('data-cardType');
	}	
	});
*/
	console.log($argTypes.length);
	$("#answerBoard").html($argTypes.length);		
});

$(document).on('mouseenter', '#comandLine', function() {
	$( ".slotEmpty" ).droppable();
	$( "#comandLine .bbgCard,#comandLine .bbgFunction" ).draggable({
      revert: "invalid", // when not dropped, the item will revert back to its initial position
      containment: "document",
      helper: "original",
      cursor: "move"
    });
	console.log("refreshato");
		
});
 

  
  
  /*
  
  $( function() {
 
    // There's the gallery and the trash
    var $gallery = $( "#gallery" ),
      $trash = $( "#trash" );
 
    // Let the gallery items be draggable
    $( "li", $gallery ).draggable({
      cancel: "a.ui-icon", // clicking an icon won't initiate dragging
      revert: "invalid", // when not dropped, the item will revert back to its initial position
      containment: "document",
      helper: "clone",
      cursor: "move"
    });
 
    // Let the trash be droppable, accepting the gallery items
    $trash.droppable({
      accept: "#gallery > li",
      classes: {
        "ui-droppable-active": "ui-state-highlight"
      },
      drop: function( event, ui ) {
        deleteImage( ui.draggable );
      }
    });
 
    // Let the gallery be droppable as well, accepting items from the trash
    $gallery.droppable({
      accept: "#trash li",
      classes: {
        "ui-droppable-active": "custom-state-active"
      },
      drop: function( event, ui ) {
        recycleImage( ui.draggable );
      }
    });
 
    // Image deletion function
    var recycle_icon = "<a href='link/to/recycle/script/when/we/have/js/off' title='Recycle this image' class='ui-icon ui-icon-refresh'>Recycle image</a>";
    function deleteImage( $item ) {
      $item.fadeOut(function() {
        var $list = $( "ul", $trash ).length ?
          $( "ul", $trash ) :
          $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );
 
        $item.find( "a.ui-icon-trash" ).remove();
        $item.append( recycle_icon ).appendTo( $list ).fadeIn(function() {
          $item
            .animate({ width: "48px" })
            .find( "img" )
              .animate({ height: "36px" });
        });
      });
    }
 
    // Image recycle function
    var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='ui-icon ui-icon-trash'>Delete image</a>";
    function recycleImage( $item ) {
      $item.fadeOut(function() {
        $item
          .find( "a.ui-icon-refresh" )
            .remove()
          .end()
          .css( "width", "96px")
          .append( trash_icon )
          .find( "img" )
            .css( "height", "72px" )
          .end()
          .appendTo( $gallery )
          .fadeIn();
      });
    }
 
    // Image preview function, demonstrating the ui.dialog used as a modal window
    function viewLargerImage( $link ) {
      var src = $link.attr( "href" ),
        title = $link.siblings( "img" ).attr( "alt" ),
        $modal = $( "img[src$='" + src + "']" );
 
      if ( $modal.length ) {
        $modal.dialog( "open" );
      } else {
        var img = $( "<img alt='" + title + "' width='384' height='288' style='display: none; padding: 8px;' />" )
          .attr( "src", src ).appendTo( "body" );
        setTimeout(function() {
          img.dialog({
            title: title,
            width: 400,
            modal: true
          });
        }, 1 );
      }
    }
 
    // Resolve the icons behavior with event delegation
    $( "ul.gallery > li" ).on( "click", function( event ) {
      var $item = $( this ),
        $target = $( event.target );
 
      if ( $target.is( "a.ui-icon-trash" ) ) {
        deleteImage( $item );
      } else if ( $target.is( "a.ui-icon-zoomin" ) ) {
        viewLargerImage( $target );
      } else if ( $target.is( "a.ui-icon-refresh" ) ) {
        recycleImage( $item );
      }
 
      return false;
    });
  } );
  
 */