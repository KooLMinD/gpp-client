var levels = [
  {
    helpTitle : "Seleziona elementi in base al loro tipo",
    selectorName : "Selettore di tipi (type Selector)",
    doThis : "Seleziona tutti i piatti",
    selector : "plate",
    syntax : "A",
    help : "Seleziona tutti gli elementi di tipo <strong>A</strong>. Tipo si riferisce al tipo di tag, quindi <tag>div</tag>, <tag>p</tag> e <tag>ul</tag> sono tutti tipi diversi.",
    examples : [
      '<strong>div</strong> seleziona tutti i <tag>div</tag>.',
      '<strong>p</strong> seleziona tutti i <tag>p</tag>.',
    ],
    boardMarkup: `
    <plate/>
    <plate/>
    `
  },
  {
    doThis : "Seleziona il piatto decorato",
    selector : "#fancy",
    selectorName: "Selettore di ID",
    helpTitle: "Seleziona l'elemento con un certo ID, solo un elemento con un certo ID é consentito in un file.",
    syntax: "#id",
    help : 'Seleziona l\'elemento con l\'id <strong>id</strong>. Puoi anche combinare il selettore di ID con quello di tipo.',
    examples : [
      '<strong>#cool</strong> seleziona l\'elemento  con <strong>id="cool"</strong>',
      '<strong>ul#long</strong> seleziona <tag>ul id="long"</tag>'
    ],
    boardMarkup : `
    <plate id="fancy"/>
    <plate/>
    <bento/>
    `
  }
];
