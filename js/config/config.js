/* Configuration for gpp core interface (API) */

	//COSTANTS
	var APIBASE = 'http://localhost/gpp-core/public/api/v1';
	var TOKEN = null;
	var LANG = 'it';

  //ENDPOINTS
	var endpoint = {
		login : APIBASE+"/login" ,
		users : {
			list: APIBASE+"/users"
		},
		lobby : APIBASE+"/quizzes" 
	};