/* Author			: Roberto Taglialatela, Alessandro Fioroni
   Creation date	: 20.03.2018
   Last updated		: 04.05.2018
   Content			: user registration function
*/

	function gppRegister(){
		$.ajax({
			 url: endpoint.register,
 			 type: "POST",
			 beforeSend: function(request) {
				console.log('calling URL: ' +endpoint.register);
				request.setRequestHeader("X-localization", LANG);
			 },
			 data: {
				 first_name: $('#reg-fname').val(),
				 last_name: $('#reg-lname').val(),
				 nickname: $('#reg-nickname').val(), 
				 email: $('#reg-email').val(),
				 password: $('#reg-password').val(),
				 repeat_password: $('#reg-repeat-password').val()
			 },
			 success: function(response) { 
			 console.log(response);
			  if(response.status=="success"){
				// remove previous validation errors
				$('#reg-errors').remove();
				$('#reg-success').css('display','block').append($('<p></p>').text(response.message));
			  }
			  if(response.status=="fail"){
				// empty previous validation errors
				$('#reg-errors').empty();
				//registration failed, show validation messages
				$.each(response.errors.validation, function(index,error){
					console.log(error);	
					$('#reg-errors').css('display','block').append($('<p></p>').text(error));
					// TODO add color to fields with errors
				});	
			  }
			 },
			 error: function(jqXHR, exception) {
				console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
		}
	  
$( document ).ready(function() {
	
	//On registration submit, register account
	$( '#register-btn' ).on( "click", gppRegister);	
});	
	