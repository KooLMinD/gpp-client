/* Author			: Roberto Taglialatela
   Creation date	: 17.06.2018
   Last updated		: 15.08.2018
   Content			: manage quiz creation (quiz, answers)
*/

	
$(document).ready(function() {
		
	/*-----------------------------------------------------------------------------------------------*/
	/*------------------------------------   NEW-QUIZ.HTML   ----------------------------------------*/
	/*-----------------------------------------------------------------------------------------------*/
	
	// get session id from local storage
	var user 			= JSON.parse(localStorage.getItem('USER'));
	var userId			= user.id;
	var privateQuiz 	= 0;
	var language 		= 1;
	var title 			= null;
	var description 	= null; 
	var questions 		= [];
	var questionNr		= 0;

	// add new answer when button is pressed
	$('button#add-answer').on('click',function () {
		createQuestionItem();
	});
	
	/* launch create quiz function when submit button is clicked */
	$('#new-quiz-btn2').on('click',function () {
		createNewQuiz();
	});

	// manage private quiz checkbox
	$('#new-quiz-form #private-quiz').on('change', function() {
		privateQuiz = this.checked ? 1 : 0;
	});	
	
	/*
	$('#new-quiz-form').on('focus','input:not(.question-dynamic)',function(){
		console.log('scrolling form');
		$(this).scrollView();
	})
		
	$.fn.scrollView = function () {
	  return this.each(function () {
		$('html, body').animate({
		  scrollTop: $(this).offset().top - 100
		}, 700);
		console.log('offset is: ' + $(this).offset().top);
	  });
	}
	*/
	function createNewQuiz(){
			// get form fields
			title = $('#new-quiz-form #title').val();
			description = $('#new-quiz-form #description').val();
			language = $('#new-quiz-form #language option:selected').attr('data-value');
			questions = [];
			$(".questions").each(function() {
				var question = {};
				var answers  = [];
				$(this).find(".answer").each(function() {
					var answer = {}
					// if the answer has a text, add it to question
					if( $(this).find('[class~=answer-text]').val() ) {
						answer ["text"] = $(this).find('[class~=answer-text]').val();
						// add correct field from checkbox
						var answerCorrect = 0;
						if ($(this).find('[class~=answer-correct]').is(':checked')) {
							 answerCorrect = 1;
						}
						answer ["correct"] = answerCorrect;
						answers.push(answer);
					}
				});
				question ["text"] = $(this).find('[class~=question-title]').val();
				question ["answers"] = answers;
				
				questions.push(question);			
			});
			var quizObj = {};
			quizObj ['user_id'] = userId;
			quizObj ['title'] = title;
			quizObj ['description'] = description;
			quizObj ['language'] = language;
			quizObj ['private'] = privateQuiz;
			quizObj ['questions'] = questions;
			console.log('quiz is: ');
			console.log(quizObj);	
			// send quiz
			$.ajax({
				 url: endpoint.createQuiz,
				 type: "POST",
				 beforeSend: function(request) {
					TOKEN = localStorage.getItem('TOKEN');
					request.setRequestHeader("X-localization", LANG);
					request.setRequestHeader("Api-Token", TOKEN);
					console.log("url called: " + endpoint.createQuiz + ' [POST]');
				 },
				 data: {
					 user_id			: userId,
					 language_id		: language,
					 title				: title,
					 description		: description,
					 private			: privateQuiz,
					 questions			: questions
				 },
				 success: function(response) { 
				    if (response.status == 'success') {
						console.log('quiz created successfully');
						showAlert('success','quiz creato, buon divertimento!',2000);
						// return to quiz list
						setTimeout(function(){ window.location.href = "quiz-list.html"; }, 3000);
					} else if (response.status == 'fail') {
						console.log('validation failed');
						console.log(response);
						showAlert('warning','ci sono dei campi non validi, controlla');
						$.each(response.errors.validation, function(field, value) {
							console.log(field + ':' + value);
							$('#'+field).closest('.form-group').find('.invalid-feedback > h5').text(value);
						});

					}	  					
				 },
				 error: function(jqXHR, exception) {
					showAlert('danger','Ups, c\'é stato un errore, ritenta');
					console.log("got errors: " + jqXHR.message);
					console.log(jqXHR.status);
				 }
			  });
	}
			
	function createQuestionItem() {	
		  // calculate nr of questions to generate custom id
		  questionNr =  $(".questions").length+1;	
		  // create new answer, generate input and label unique id (question nr/answer nr)
		  var questionRow 		= $('<div></div>').addClass('form-row questions');
		  var questionCol 		= $('<div></div>').addClass('col-md-12 mb-3').append($('<div></div>').addClass('form-group question'));
		  var questionInput		= $('<input />').addClass('form-control form-control-lg question-title question-dynamic').attr({type: "text", placeholder: "Inserisci il testo della domanda "+ questionNr, required: 'required'});
		  var answerRow1		= $('<div></div>').addClass('col-md-6 mb-3').append($('<div></div>').addClass('form-row').append($('<div></div>').addClass('col-md-10').append($('<div></div>').addClass('form-group answer').append($('<input />').addClass('form-control form-control-lg answer-text question-dynamic').attr({type: "text", id: 'answer'+questionNr+'.1', placeholder: "Risposta 1", required: 'required'})))).append($('<div></div>').addClass('col-md-2').append($('<div></div>').addClass('form-group answer text-center').append($('<input />').attr({type:"checkbox", id:'correct'+questionNr+'.1',checked: 'checked'})).append($('<label></label>').attr('for','correct'+questionNr+'.1')))));
		  var answerRow2		= $('<div></div>').addClass('col-md-6 mb-3').append($('<div></div>').addClass('form-row').append($('<div></div>').addClass('col-md-10').append($('<div></div>').addClass('form-group answer').append($('<input />').addClass('form-control form-control-lg answer-text question-dynamic').attr({type: "text", id: 'answer'+questionNr+'.2', placeholder: "Risposta 2", required: 'required'})))).append($('<div></div>').addClass('col-md-2').append($('<div></div>').addClass('form-group answer text-center').append($('<input />').attr({type:"checkbox", id:'correct'+questionNr+'.2'})).append($('<label></label>').attr('for','correct'+questionNr+'.2')))));
		  var answerRow3		= $('<div></div>').addClass('col-md-6 mb-3').append($('<div></div>').addClass('form-row').append($('<div></div>').addClass('col-md-10').append($('<div></div>').addClass('form-group answer').append($('<input />').addClass('form-control form-control-lg answer-text question-dynamic').attr({type: "text", id: 'answer'+questionNr+'.3', placeholder: "Risposta 3"})))).append($('<div></div>').addClass('col-md-2').append($('<div></div>').addClass('form-group answer text-center').append($('<input />').attr({type:"checkbox", id:'correct'+questionNr+'.3'})).append($('<label></label>').attr('for','correct'+questionNr+'.3')))));
		  var answerRow4		= $('<div></div>').addClass('col-md-6 mb-3').append($('<div></div>').addClass('form-row').append($('<div></div>').addClass('col-md-10').append($('<div></div>').addClass('form-group answer').append($('<input />').addClass('form-control form-control-lg answer-text question-dynamic').attr({type: "text", id: 'answer'+questionNr+'.4', placeholder: "Risposta 4"})))).append($('<div></div>').addClass('col-md-2').append($('<div></div>').addClass('form-group answer text-center').append($('<input />').attr({type:"checkbox", id:'correct'+questionNr+'.4'})).append($('<label></label>').attr('for','correct'+questionNr+'.4')))));
		  var questionWrapper	= questionRow.append(questionCol.append(questionInput))
											 .append(answerRow1)
											 .append(answerRow2)
											 .append(answerRow3)
											 .append(answerRow4);
		  $('#quiz-questions').append($('<hr>')).append(questionWrapper);
		  console.log('dynamic question is:');
		  console.log(questionWrapper);
		  // after adding new question, scoll to bottom and set focus on question input
		  $("html, body").animate({scrollTop: $('html, body').get(0).scrollHeight}, 1000);
		  $('.question-title:last-of-type').focus();
	}
  
// END document.ready
});	
	
