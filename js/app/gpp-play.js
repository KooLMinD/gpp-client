/* Author			: Roberto Taglialatela
   Creation date	: 29.04.2018
   Last updated		: 19.07.2018
   Content			: manage play workflow (questions, answers, score)
*/

$(document).ready(function() {
		
	/*-----------------------------------------------------------------------------------------------*/
	/*------------------------------------   PLAY.HTML   --------------------------------------------*/
	/*-----------------------------------------------------------------------------------------------*/
	
	// get session id from local storage
	var session = JSON.parse(localStorage.getItem('SESSION_OBJ'));
	var sessionId = session.id;
	// get user id from local storage
	var user = JSON.parse(localStorage.getItem('USER'));
	userId = user.id;
	var questionText = null;
	var questionId = null;
	var userAnswers = [];
	var questionScore = 0;
	var trophiesShown = false;
	console.log('session id is:' + sessionId);
	playSession(sessionId);	
	
	// manage single/multiple choice answers
    $('#answer-btn').on('click', '.btn-wrapper', function () {
		// if question is single choice, only one answer can be chosen, if multiple choice any number of answers can be selected
		var activeQuestion = JSON.parse(localStorage.getItem('QUESTION'));
		console.log(activeQuestion);
		console.log('is multiple choice? '+ activeQuestion.question_data.multiple_choice);
		if (activeQuestion.question_data.multiple_choice) {
			$(this).toggleClass('btn-pressed');
		} else {
			$('.btn-wrapper').removeClass('btn-pressed');
			$(this).toggleClass('btn-pressed');
		}
		// if at least one question is selected, activate end answer button
		console.log('answers pressed are '+$('.btn-pressed').length);
		$('.btn-pressed').length > 0 ? $('button#end-answer').addClass('ready') : $('button#end-answer').removeClass('ready');
	});
	
	// send answer when clicking end answer button
	$('button#end-answer').on('click',function () {
		if ($(this).is(".ready")) {
			sendAnswer(questionId, userAnswers)} ;
	});
	
	function playSession(sessionId){
			$.ajax({
				 url: endpoint.sessionLobby + sessionId + '/play',
				 type: "POST",
				 beforeSend: function(request) {
					TOKEN = localStorage.getItem('TOKEN');
					request.setRequestHeader("X-localization", LANG);
					request.setRequestHeader("Api-Token", TOKEN);
				 },
				 data: {
					 user_id			: userId,
					 quiz_session_id	: sessionId
				 },
				 success: function(response) { 
					console.log(response);
					// loop play function until end status
					  if (response.status_code == 'STARTING'){
						  $('#play-screen').hide();
						  $('#starting-countdown').show();
						  // manage countdown timer
						  if (response.data.seconds_left < 2) {
						    $('#starting-countdown').text('VIA!').slideDown(500);								  
						  } else if (response.data.seconds_left < 4) {
							$('#starting-countdown').text('PARTENZA').slideDown(500); 
						  } else if (response.data.seconds_left < 6) {
							$('#starting-countdown').text('PRONTI?').slideDown(500); 
						  }					  
					  } else if (response.status_code == 'QUESTION_PREVIEW'){
						  console.log('status is: '+response.status_code);
						  // save question to local storage
						  question = JSON.stringify(response);
						  questionId = response.question_status.id;
						  localStorage.setItem('QUESTION', question);
						  $('#starting-countdown').hide();
						  $('#time-counter').hide();
						  $('#feedback').hide();
						  $('#end-answer').hide();
						  $('#play-screen').show();
						  // manage countdown timer
						  $('#time-counter').text(response.question_status.time_left);
						  // load new question text
						  loadQuestion(response);						
					  } else if (response.status_code == 'QUESTION_ACTIVE'){
						  // manage countdown timer
						  $('#time-counter').show();
						  $('#time-counter').text(response.question_status.time_left);
						  // if question is changed update data
						  if(response.question_data.text != questionText) {
							// reset and show confirm answer button
							$('button#end-answer').removeClass('ready');
							$('#end-answer').show();
							// load new question text
							questionText = response.question_data.text;
							loadAnswers(response);					
						  }									
					  } else if (response.status_code == 'QUESTION_OVER'){
						  console.log('status is: '+response.status_code);
						  $('#time-counter').hide();
						  $('#end-answer').hide();
						  showAnswerResults(response);
					  } else if (response.status_code == 'TIME_BREAK'){
						  console.log('status is: '+response.status_code);
						  console.log(response);
						  // time break between questions, show question results
						  getScore(response);
						  $('#play-screen').hide();
						  $('#feedback').show();
					  } else if (response.status_code == 'QUIZ_OVER'){
						   console.log('status is: ' + response.status_code);
						   console.log(response);
						  // session is over, show final standings
						  createPodium(response);
						  $('#play-screen').hide();
						  $('#feedback').hide();
						  $('#final-feedback').show();
						  // notify achieved trophies
						  if (!trophiesShown) {
							showTrophies();							  
						  }		
						  // clear local storage
						  localStorage.removeItem('SESSION_ID');
						  localStorage.removeItem('SESSION_OBJ');
					  }
				 },
				 complete: function() {
					  if(question.status_code != 'QUIZ_OVER') {
						setTimeout(function () {playSession(sessionId);}, 200);	
					  }	
				 },
				 error: function(jqXHR, exception) {
					console.log("got errors: " + jqXHR.message);
					console.log(jqXHR.status);
					console.log(jqXHR.responseText);

				 }
			  });
			}
	function loadQuestion(question) {	 
		  // set question text
		  $('#question').text(question.question_data.text);
		  $('.btn-wrapper').remove();	
	}
	
	function loadAnswers(question) {
		  // set question text
		  $('#question').text(question.question_data.text);		
		  // set question answers
		  $.each(question.question_data.answers, function(index,answer){
			var answerNr = ++index;
			var buttonWrapper = $('<div></div>').addClass('btn-wrapper col-xs-12 col-md-6').attr({'data-answer-id':answer.id});
			var answerButton = $('<button></button>').addClass('btn-answer').attr('id','answer' + answerNr).attr('data-answer-id', answer.id).text(answer.text);
			$('#answer-btn').prepend(buttonWrapper.append(answerButton));				  
		  });	
	}
	  
	  function sendAnswer(questionId, userAnswer) {	 
	    // hide confirm answer button
		$('#end-answer').hide();
		// collect all clicked answers
		$('.btn-pressed').each(function(index, value) {
			answerId = $(this).find('button').attr('data-answer-id');
			// add answer id to answers array
			userAnswers[index] = answerId;
		});
		//console.log('question id: ' + questionId);
		//console.log('answer array: ' + userAnswers);
		// send answer
			$.ajax({
				 url: endpoint.sessionLobby + sessionId + '/answer',
				 type: "POST",
				 beforeSend: function(request) {
					TOKEN = localStorage.getItem('TOKEN');
					request.setRequestHeader("X-localization", LANG);
					request.setRequestHeader("Api-Token", TOKEN);
					console.log('calling url: ' + endpoint.sessionLobby + sessionId + '/answer' + ' [POST]');
				 },
				 data: {
					 question_id	: questionId,
					 answer_id		: userAnswers
				 },
				 success: function(response) { 
				    console.log('answer sent correctly');
				 },
				 error: function(jqXHR, exception) {
					console.log("got errors: " + jqXHR.text);
					console.log('error.status' + jqXHR.status);
				 }
			});	
	  }
	  
	  function showAnswerResults(results) {
		  	console.log('showing correct answers');
			console.log(results);
		  // show answers results
		  $.each(results.question_data.answers, function(index, answer){ 
			$('.btn-wrapper').each(function(index, button){ 
				if ($(this).attr('data-answer-id') == answer.id && answer.correct  == 1) {
					console.log('answer is correct');
					$(this).find('.btn-answer').addClass('play-answer-correct');
				}
		    });
		  });
	  }
	  
	  
	  function getScore(response) {
		// empty standings list
		$('#session-standing-list').empty();

		// create team standings
		$.each(response.data.leaderboard.data.team_leaderboard, function(index,teams){
		  //console.log('position ' + teams.position + 'team ' + teams.team + ': score ' + teams.score);
		  createStandingItem(index, teams);
		});
	  }
	  
	  function createPodium(response) {
	    // TODO create podium
	    $('#session-standing-list').empty();
		$(response.data.leaderboard.data.team_leaderboard).each(function (index, teams) {
		  if (index == 0) {
			$('#first-place-team').text(teams.team); 
		  } else if (index == 1) {
			$('#second-place-team').text(teams.team);
		  } else if (index == 2) {
			$('#third-place-team').text(teams.team); 
		  }
		});
	  }
	  
	  function createStandingItem(index, team) {
		  
		  // create existing quiz list item
		  index = ++index;
		  var teamItem 	= $('<li></li>').addClass('quiz-item')
							.append($('<div></div>').addClass('row')
								.append($('<div></div>').addClass('col-md-2 position text-center')
									.append($('<div></div>').text(team.position)))
								.append($('<div></div>').addClass('col-md-6 team-name text-left')
									.append($('<div></div>').text(team.team)))
								.append($('<div></div>').addClass('col-md-4 team-score text-center')
									.append($('<div></div>').text(team.score))));
		  $('#session-standing-list').append(teamItem);
	  }
	  
	  /*
		<li class=" team-score-item">
			<div class="row">
				<div id="" class="d-none d-sm-block col-sm-2">
				  1
				</div>
				<div id="" class="col-8 col-sm-6 col-md-6 text-left">
				  Tiubez
				</div>
				<div id="" class="col-4 col-md-4">
				  2780
				</div>
			</div>
		</li>
		*/
		
	function showTrophies() {	 
		// check achieved trophies
		$.ajax({
			 url: endpoint.trophiesAchieved,
			 type: "GET",
			 beforeSend: function(request) {
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
				console.log('calling trophies achieved: ' + endpoint.trophiesAchieved + ' [GET]');
			 },
			 success: function(response) { 
				console.log('achieved trophies');
				console.log(response);
				// pop achieved trophies
				$(response.data.trophies).each(function (index, trophy) {
					var trophyDifficultiy = null;
					switch(trophy.difficulty) {
						case 'easy':
							trophyDifficultiy = 'bronze';
							break;
						case 'medium':
							trophyDifficultiy = 'silver';
							break;
						case 'hard':
							trophyDifficultiy = 'gold';
							break;
						default:
							return false;
						  }			  
					// show notification for each achieved trophy
					setTimeout(function () {
						$.notify({
							icon: trophy.icon,
							title: trophy.title,
							message: trophy.description,
							url : 'trophies.html#trophy'+trophy.id
						}, {
							type: 'trophy-'+trophyDifficultiy,
							onShow: trophyAudio()
						});
					}, 500+3000*index);
				});	
				trophiesShown = true;
				// TODO create API to set trophy already shown in this session
			 },
			 error: function(jqXHR, exception) {
				console.log("got errors: " + jqXHR.text);
				console.log('error.status' + jqXHR.status);
			 }
		});	
	}
	
	/* notification configuration */
	$.notifyDefaults({
		delay: 7000,
		mouse_over: 'pause',
		icon_type: 'class',
		spacing: -30,
		offset: {
			x: 0,
			y: 10
		},
		placement: {
			from: "top"
		},
		animate: {
			enter: "animated fadeInDown",
			exit: "animated fadeOutUp"
		},
		template: 	
			'<div class="item col-xs-12 col-sm-10 col-md-8 col-lg-5 alert alert-{0}" data-notify="container">' +
			  '<a class="trophyLink" href="{3}">' +
				'<div class="container notification-wrapper">' +
					'<div class="row v-middle">' +
						'<div class="col-3 trophy-popup-left" >' +
							'<div class="trophy-wrapper">' +
								'<div class="unlocked">' +
									'<i class="fas fa-2x " data-notify="icon"></i>' +
								'</div>' + 
							'</div>' +
						'</div>' +
						'<div class="col-9 trophy-popup-right">' +
							'<h4>{1}</h4>' +
							'<p>{2}</p>' +
						'</div>' +
					'</div>' +
				'</div>' +
			  '</a>' +
			'</div>'
	});

	function trophyAudio () {
		var trophyPop = new Audio('assets/audio/trophy3.mp3');
		trophyPop.play();
	}
	
// END document.ready
});	
	

