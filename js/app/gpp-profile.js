/* Author			: Roberto Taglialatela
   Creation date	: 27.07.2018
   Last updated		: 28.07.2018
   Content			: manage profile (public data, private data)
*/

$(document).ready(function() {
		
	/*-----------------------------------------------------------------------------------------------*/
	/*------------------------------------   PROFILE.HTML   ----------------------------------------*/
	/*-----------------------------------------------------------------------------------------------*/
	
	// get session id from local storage
	var user 			= JSON.parse(localStorage.getItem('USER'));
	var userId			= user.id;
	
	// set profile data on profile form
	$(function() {
		$('#nickname').val(user.nickname);
		$('#first_name').val(user.first_name);
		$('#last_name').val(user.last_name);
		$('#email').val(user.email);	
    });	

	// update profile public data when button is pressed
	$( "#form-private-profile" ).submit(function( event ) {
	  console.log( "Handler for .submit() called." );
	  event.preventDefault();
	});
	$('#update-public-profile').on('click',function () {
		updatePublicProfile();
	});
	
	// update profile private data when button is pressed
	$('#update-private-profile').on('click',function () {
		// get passwords
		var oldPassword = $('#password').val();
		var newPassword = $('#new-password').val();
		var newPasswordConfirm = $('#new-password-confirm').val();
		console.log(oldPassword+'/'+newPassword +'/'+newPasswordConfirm);
		if (newPassword && newPasswordConfirm && newPassword) {
			console.log(newPassword +'/'+newPasswordConfirm);
			if (newPasswordConfirm == newPassword) {
			console.log('trying to update pwd');
			updatePassword(newPassword, newPasswordConfirm);
			} else {
				showAlert('warning','le nuove password non coincidono');
			}
		} else {
			showAlert('warning','compila tutti i campi per modificare la password');
		}
	});
	
	function updatePublicProfile(){
		// get nickname
		var nickname = $('#nickname').val();
		// send profile data
		$.ajax({
			 url: endpoint.updateProfile + userId + '/basic',
			 type: "PATCH",
			 beforeSend: function(request) {
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
				console.log('url called: ' + endpoint.updateProfile + userId + '/basic' + ' [PATCH]');
			 },
			 data: {
				 nickname			: nickname
			 },
			 success: function(response) { 
				console.log(response);
				// return to quiz list
				//window.location.href = "quiz-list.html";
				
			 },
			 error: function(jqXHR, exception) {
				console.log("got errors: " + jqXHR.message);
				console.log(jqXHR.status);
			 }
		});
	}
	
	function updatePassword(newPassword, newPasswordConfirm){
		// send profile data
		$.ajax({
			 url: endpoint.updateProfile + userId + '/password',
			 type: "PATCH",
			 beforeSend: function(request) {
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
				console.log('url called: ' + endpoint.updateProfile + userId + '/password' + ' [PATCH]');
			 },
			 data: {
				 password : newPassword,
				 repeat_password : newPasswordConfirm
			 },
			 success: function(response) { 
				console.log(response);
				showAlert('success','password modificata');
			 },
			 error: function(jqXHR, exception) {
				console.log("got errors: " + jqXHR.message);
				console.log(jqXHR.status);
			 }
		});
	}
	
// END document.ready
});	
	
