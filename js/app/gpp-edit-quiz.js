/* Author			: Roberto Taglialatela
   Creation date	: 29.06.2018
   Last updated		: 26.07.2018
   Content			: manage quiz update (quiz, answers)
*/

$(document).ready(function() {
		
	/*-----------------------------------------------------------------------------------------------*/
	/*------------------------------------   EDIT-QUIZ.HTML   --------------------------------------------*/
	/*-----------------------------------------------------------------------------------------------*/

	// get session id from local storage
	TOKEN = localStorage.getItem('TOKEN');
	var user 			= JSON.parse(localStorage.getItem('USER'));
	var userId			= user.id;
	var quizId			= localStorage.getItem('UPDATE_QUIZ_ID');
	var title			= null;
	var description		= null;
	var language		= 1;
	var privateQuiz		= 0;
	var questions 		= [];
	var questionNr		= 0;
	var answerNr 		= 0;
	getQuiz(quizId);
	
	// set questions list as sortable
	$(function() {
         $('#questions-list').sortable();
    });			  
	
      // set focus on first input field
	$(function() {
		$('#new-quiz-form #title').focus();
    });	
	
	// add new question and reset new  when button is pressed
	$('button#add-question').on('click',function () {
		getNewQuestion();
		clearNewQuestionForm();
	});
	
	/* launch edit quiz function when submit button is clicked */
	$('#edit-quiz-btn2').on('click',function () {
		  editQuiz(quizId);
	});
	
	// manage private quiz checkbox
	$('#private-quiz').on('change', function() {
		privateQuiz = this.checked ? 1 : 0;
	});	
	
	// delete question when button is pressed
	$('#questions-list').on('click','.quiz-manage-buttons [class~=delete]', function () {
		deleteQuestion(this);
	});
	
	// duplicate question when button is pressed
	$('#questions-list').on('click','.quiz-manage-buttons [class~=duplicate]', function () {
		duplicateQuestion(this);
	});
	
	
	function getQuiz(quizId){
			$.ajax({
				 url: endpoint.createQuiz  + "/" + quizId,
				 type: "GET",
				 beforeSend: function(request) {
					TOKEN = localStorage.getItem('TOKEN');
					request.setRequestHeader("X-localization", LANG);
					request.setRequestHeader("Api-Token", TOKEN);
					console.log("url called: " + endpoint.createQuiz + "/" + quizId + ' [GET]');
				 },
				 success: function(response) { 
				  	console.log(response);
					// TODO fill form with quiz data
					$('#title').val(response.data.quiz.title);
					$('#description').text(response.data.quiz.description);
					// set private checkbox as checked if quiz is private
					if(response.data.quiz.private == 1) {
						$('#private-quiz').attr('checked','checked');
					}
					// set language option as selected
					$('#language option').each(function() {
						$(this).removeAttr('selected');
						if($(this).attr('data-value') == response.data.quiz.language_id) {
							$(this).attr('selected', 'selected');
						}
					});
					
					// TODO create question box for each existing question
					$.each(response.data.quiz.questions, function(index,question){
						// add player to list
						createQuestionItem(question, index);
					});	
				 },
				 error: function(jqXHR, exception) {
					console.log("got errors: " + jqXHR.message);
					console.log(jqXHR.status);
				 }
			  });
			}
	
	function editQuiz(quizId){
		// get form fields
		title = $('#edit-quiz-form #title').val();
		description = $('#edit-quiz-form #description').val();
		language = $('#edit-quiz-form #language option:selected').attr('data-value');
		questions = [];
		$(".questions").each(function() {
			var question = {};
			var answers  = [];
			question ["text"] = $(this).find('[class~=question-title]').val();
			$(this).find(".answer").each(function() {
				var answer = {}
				if( $(this).find('[class~=answer-text]').val() ) {
					answer ["text"] = $(this).find('[class~=answer-text]').val();
					// TODO add correct field
					var answerCorrect = 0;
					if ($(this).find('[class~=answer-correct]').is(':checked')) {
						 answerCorrect = 1;
					}
					answer ["correct"] = answerCorrect;
					answers.push(answer);
				}				
			});
			question ["answers"] = answers;
			questions.push(question);	
		});
		console.log(questions);

		$.ajax({
			 url: endpoint.editQuiz + quizId,
			 type: "PUT",
			 beforeSend: function(request) {
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
				console.log("url called: " + endpoint.editQuiz + quizId + ' [PUT]');
			 },
			 data: {
				 user_id			: userId,
				 title				: title,
				 description		: description,
				 private			: privateQuiz,
				 language_id		: language,
				 questions			: questions
			 },
			 success: function(response) { 
				console.log(response);
				// return to quiz list
				window.location.href = "quiz-list.html";
				
			 },
			 error: function(jqXHR, exception) {
				console.log("got errors: " + jqXHR.message);
				console.log(jqXHR.status);
			 }
		  });
		}
			
	function createQuestionItem(question, index) {
		  // create variables for unique question id and answer id
		  var questionNr = ++index;
		  var answerTot = question.answers.length;
		  var answerEmpty = 4-answerTot
		  // create existing question list item
		  var answerRow		= $('<div></div>').addClass('row');
		  var answerCol 	= null;
		  $.each(question.answers, function(index,answer){
			answerNr = ++index;
			// create answer col
			var answerCheckbox	= $('<input />').addClass('form-check-input answer-correct').attr({type:"checkbox", id:"correct"+questionNr+'.'+answerNr});
			if (answer.correct == '1') {
				answerCheckbox.attr('checked','checked');
			}
			var answerCheckboxLabel = $('<label></label>').attr({for:"correct"+questionNr+'.'+answerNr});
			var answerInput	= $('<input >').addClass('form-control form-control-lg answer-text').attr({required:"required"}).val(answer.text);
			answerCol		= $('<div></div>').addClass('col-md-12 row answer').append($('<div></div>').addClass('form-check col-sm-2 col-md-1 centered').append(answerCheckbox).append(answerCheckboxLabel)).append($('<div></div>').addClass('form-check col-sm-10 col-md-11').append(answerInput));
			answerRow.append(answerCol);
		  });
		  // if there are less than 4 answer input, add empty ones
		  for (var i = 0; i < answerEmpty; i++) {
			answerNr = 4-answerEmpty+1+i;
			var answerEmptyCheckbox		= $('<input />').addClass('form-check-input answer-correct').attr({type:"checkbox", id:"correct"+questionNr+'.'+answerNr});
			var answerEmptyCheckboxLabel= $('<label></label>').attr({for:"correct"+questionNr+'.'+answerNr});
			var answerEmptyInput		= $('<input >').addClass('form-control form-control-lg answer-text').attr({placeholder:'inserisci il testo della risposta'});
			var answerEmptyCol			= $('<div></div>').addClass('col-md-12 row answer').append($('<div></div>').addClass('form-check col-sm-2 col-md-1 centered').append(answerEmptyCheckbox).append(answerEmptyCheckboxLabel)).append($('<div></div>').addClass('form-check col-sm-10 col-md-11').append(answerEmptyInput));
			answerRow.append(answerEmptyCol);
		  } 
		  var answersCollapsePanel =  $('<div></div>').append($('<div></div>').addClass('panel-heading')
														.append($('<h4></h4>').addClass('panel-title')
															.append($('<a></a>').addClass('collapsed').attr({"data-toggle":"collapse", "data-target":"#collapse"+questionNr}).text('risposte'))))
													  .append($('<div></div>').addClass('panel-collapse collapse').attr({id:'collapse'+questionNr})
															.append($('<div></div>').addClass('panel-body'))
																.append(answerRow));
		  var questionTitle		= $('<div></div>').addClass('col-md-12 form-group').append($('<input>').addClass('form-control form-control-lg question-title ').attr({id:"question"+questionNr, required:"required"}).val(question.text));
		  var questionButtons	= $('<div></div>').addClass('container')
										.append($('<div></div>').addClass('row justify-content-center quiz-manage-buttons')
													.append($('<div></div>').addClass('col-6 col-sm-2 col-md-1 text-center duplicate')
														.append($('<div></div>').addClass('button-circle-wrapper').attr({role:'button'})
															.append($('<div></div>').addClass('button-circle')
																.append($('<i></i>').addClass('fa fa-copy fa-2x')))))
													.append($('<div></div>').addClass('col-6 col-sm-2 col-md-1 text-center delete')
														.append($('<div></div>').addClass('button-circle-wrapper').attr({role:'button'})
															.append($('<div></div>').addClass('button-circle')
																.append($('<i></i>').addClass('fa fa-times fa-2x'))))));
		  var questionContent	= $('<div></div>').addClass('col-md-11 content').append($('<div></div>').addClass('container').append(questionTitle).append(answersCollapsePanel));
		  var questionRow 		= $('<li></li>').addClass('questions').append($('<div></div>').addClass('container').append($('<div></div>').addClass('row').append($('<div></div>').addClass('col-md-1 drag')).append(questionContent.append(questionButtons))));
		  $('#questions-list').append(questionRow);
		}
	  
			/*
			<!-- collapsible panel -->
			  <div class="">
				<div class="panel-heading">
				  <h4 class="panel-title">
					<a href="#" class="collapsed" data-toggle="collapse"  data-target="#collapse1">
					  risposte
					</a>
				  </h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse">
				  <div class="panel-body">
					<div class="col-md-6">
						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="private">
							<label class="form-check-label" for="private">Russia</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="private" checked="checked">
							<label class="form-check-label" for="private">Marte</label>
						</div>
					</div>
				  </div>
				</div>
				<div class="row justify-content-center quiz-manage-buttons">
					<div class="col-6 col-sm-2 col-md-1 text-center duplicate-question">
						<div class="button-circle-wrapper">
							<div class="button-circle">
								<i class="fa fa-copy fa-2x"></i>
							</div>
						</div>
					</div>
					<div class="col-6 col-sm-2 col-md-1 text-center delete-question">
						<div class="button-circle-wrapper">
							<div class="button-circle">
								<i class="fa fa-times fa-2x"></i>
							</div>
						</div>
					</div>
				</div>
			  </div>
			<!-- END collapsible panel -->
			*/
			
	  function getNewQuestion() {	
	    var newQuestion = {};
	    var answers  = [];
		
		newQuestion ["text"] = $('#new-question-form').find('[class~=question-title]').val();
		$('#new-question-form .answer').each(function() {
			var answer = {}
			if( $(this).find('[class~=answer-text]').val() ) {
				answer ["text"] = $(this).find('[class~=answer-text]').val();
				// add correct field
				var answerCorrect = 0;
				if ($(this).find('[class~=answer-correct]').is(':checked')) {
					 answerCorrect = 1;
				}
				answer ["correct"] = answerCorrect;
				answers.push(answer);
			}				
		});
		newQuestion ["answers"] = answers;
		createQuestionItem(newQuestion);
	 };
	 
	 function clearNewQuestionForm() {
		// empty fields and reset focus
		$("#new-question-form")[0].reset();
		$('#new-question-form').find('[class~=question-title]').focus();
	 };
	  
	function deleteQuestion(obj){
		obj.closest('.questions').remove();
	}
	
	function duplicateQuestion(obj){
		var duplicatedQuestion = {}
		var duplicatedQuestionTitle = null;
		var duplicatedAnwers = [];
		var duplicatedAnwer = {};
		var duplicatedAnwerTitle = null;
		duplicatedQuestion ["text"] = $(obj).closest('[class~=content]').find('[class~=question-title]').val() + ' [COPIA]';
		$(obj).closest('.questions').find('[class~=answer]').each(function () {
			var answer = {}
			if( $(this).find('[class~=answer-text]').val() ) {
				answer ["text"] = $(this).find('[class~=answer-text]').val();
				// add correct field
				var answerCorrect = 0;
				if ($(this).find('[class~=answer-correct]').is(':checked')) {
					 answerCorrect = 1;
				}
				answer ["correct"] = answerCorrect;
				duplicatedAnwers.push(answer);
			}	
		});
		duplicatedQuestion ["answers"] = duplicatedAnwers;
		console.log(duplicatedQuestion);
		createQuestionItem(duplicatedQuestion);
	}
  
// END document.ready
});	
	
