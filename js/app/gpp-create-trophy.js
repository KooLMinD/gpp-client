/* Author			: Roberto Taglialatela
   Creation date	: 02.07.2018
   Last updated		: 02.07.2018
   Content			: manage trophy creation
*/

$(document).ready(function() {
		
	/*-----------------------------------------------------------------------------------------------*/
	/*------------------------------------   NEW-QUIZ.HTML   --------------------------------------------*/
	/*-----------------------------------------------------------------------------------------------*/
	
	// get session id from local storage
	TOKEN 				= localStorage.getItem('TOKEN');
	var user 			= JSON.parse(localStorage.getItem('USER'));
	var userId			= user.id;
	var secretTrophy 	= 0;
	var difficulty 		= 1;
	var title 			= null;
	var description 	= null; 
	
	
      // set focus on first input field
	$(function() {
		$('#new-trophy-form #title').focus();
    });	
	
	// add new trophy when button is pressed
	$('button#add-trophy').on('click',function () {
		createQuestionItem();
	});
	
	/* launch create trophy function when submit button is clicked */
	$('#new-trophy-btn').on('click',function () {
		createNewTrophy();
	});
	
	// manage secret trophy checkbox
	$('#new-trophy-form #private-quiz').on('change', function() {
		privateQuiz = this.checked ? 1 : 0;
	});	
	
	function createNewTrophy(){
			// get form fields
			title = $('#new-trophy-form #title').val();
			description = $('#new-trophy-form #description').val();
			difficulty = $('#new-trophy-form #difficulty option:selected').attr('data-value');			

			// send trophy
			$.ajax({
				 url: endpoint.createTrophy,
				 type: "POST",
				 beforeSend: function(request) {
					TOKEN = localStorage.getItem('TOKEN');
					request.setRequestHeader("X-localization", LANG);
					request.setRequestHeader("Api-Token", TOKEN);
					console.log("url called: " + endpoint.createTrophy + ' [POST]');
				 },
				 data: {
					 title				: title,
					 description		: description
					 //private			: secretTrophy,
					 //difficulty_id	: difficulty
				 },
				 success: function(response) { 
				  	console.log(response);
					// reset form and set focus
					//window.location.href = "quiz-list.html";
					
				 },
				 error: function(jqXHR, exception) {
					console.log("got errors: " + jqXHR.message);
					console.log(jqXHR.status);
				 }
			  });
			}
			

		//TODO after adding new question, scoll to bottom and set focus on question input
  
		//TODO manage validation with bootstrap validation class
  
  
// END document.ready
});	
	
