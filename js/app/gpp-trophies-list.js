/* Author			: Roberto Taglialatela
   Creation date	: 23.08.2018
   Last updated		: 23.08.2018
   Content			: manage trophies list
*/

$(document).ready(function() {
		
	/*-----------------------------------------------------------------------------------------------*/
	/*------------------------------------   TROPHIES-LIST.HTML   --------------------------------------------*/
	/*-----------------------------------------------------------------------------------------------*/

	// get session id from local storage
	TOKEN 				= localStorage.getItem('TOKEN');
	var user 			= JSON.parse(localStorage.getItem('USER'));
	var userId			= user.id;
	var quizId			= null;
	
	getTrophiesList();
	
	function getTrophiesList(){	
		$.ajax({
			 url: endpoint.getTrophies+userId+'/trophies/all',
			 type: "GET",
			 beforeSend: function(request) {
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
				console.log("url called: " + endpoint.getTrophies+userId+'/trophies/all' + ' [GET]');
			 },
			 success: function(response) { 
			    // reset trophy list
			    $('#trophies-list').empty();
				console.log(response);
				// create trophy box for each existing trophy
				$.each(response.data.trophies, function(index,trophy){
					createTrophyItem(trophy, index);
				});	
			 },
			 error: function(jqXHR, exception) {
				console.log("got errors: " + jqXHR.message);
				console.log(jqXHR.status);
			 }
		});
	}
				
	function createTrophyItem(trophy, index) {
		  var difficulty = null;
		  var unlocked = null;
		  var unlockDate = null;
		  var iconCode	= null;
		  
		  trophy.achieved ? unlockDate = trophy.achieved : unlockDate = '';
		  trophy.achieved ? unlocked = 'unlocked' : unlocked = 'locked';
		  trophy.achieved ? iconCode = trophy.icon : iconCode = 'fa-trophy';
		  if (unlocked == 'unlocked') {
		  switch(trophy.difficulty) {
			case 'easy':
				difficulty = 'bronze';
				break;
			case 'medium':
				difficulty = 'silver';
				break;
			case 'hard':
				difficulty = 'gold';
				break;
			default:
				return false;
			  }			  
		  }

		  // create existing quiz list item
		  var trophyIcon		= $('<div></div>').addClass('col-md-2 trophy-icon')
															.append($('<div></div>').addClass('trophy-wrapper')
																.append($('<div></div>').addClass('trophy-status ' + unlocked + ' ' + difficulty)
																	.append($('<i></i>').addClass('fa fa-2x '+ iconCode))));
		  var trophyDesc		= $('<div></div>').addClass('col-md-7 trophy-content')
															.append($('<div></div>').addClass('container')
																.append($('<div></div>').addClass('row')
																	.append($('<div></div>').addClass('col-12')
																		.append($('<h4></h4>').addClass('trophy-title').text(trophy.title))
																		.append($('<p></p>').addClass('trophy-desc').text(trophy.description)))));
		  var trophyDate		= $('<div></div>').addClass('col-md-3 trophy-details text-right')
															.append($('<div></div>').addClass('container')
																.append($('<div></div>').addClass('row')
																	.append($('<div></div>').addClass('col-12')
																		.append($('<p></p>').addClass('achievement-date').text(unlockDate))
																		.append($('<p></p>').addClass('achievement-perc')))));
		  var trophyContent		= $('<div></div>').addClass('container')
															.append($('<div></div>').addClass('row')
																.append(trophyIcon)
																.append(trophyDesc)
																.append(trophyDate));
		  var trophyItem 			= $('<li></li>').addClass('media trophy-item').attr({id:'trophy'+trophy.id}).css({'display':'none'})
												.append(trophyContent);
		  $('#trophies-list').append($(trophyItem).fadeIn(1000));
		}
		/*
		<li class="trophy-item">
				<div class="container">
					<div class="row">
						<div class="col-md-2 trophy-img">
							<div class="trophy-wrapper">
								<div class="unlocked bronze">
									<i class="fa fa-door-open fa-2x"></i>
								</div>
							</div>
						</div>
						<div class="col-md-7 trophy-content">
							<div class="container">
								<div class="row">
									<div class="col-md-10">
										<h4>Casa dolce casa</h4>
										<p>Accedi per la prima volta a GPP</p>
									</div>	
								</div>
							</div>
						</div>
						<div class="col-md-3 trophy-details text-right">
							<div class="container">
								<div class="row">
									<div class="col-md-10">
										<p>98.2%</p>
										<p>12/05/2018</p>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
		*/

// END document.ready
});	
	
