/* Author			: Roberto Taglialatela
   Creation date	: 29.04.2018
   Last updated		: 04.05.2018
   Content			: manage session lobby, for teams, before playing
*/

$(document).ready(function() {
	/*-----------------------------------------------------------------------------------------------*/
	/*------------------------------------   SESSION LOBBY.HTML   -----------------------------------*/
	/*-----------------------------------------------------------------------------------------------*/
	
	var user = JSON.parse(localStorage.getItem('USER'));
	var myself = {};
	myself ['id'] = user.id;
	myself ['team_id'] = null;
	var userId = null;
	var teamId = null;
	var userInSession = null;
	var userRole = null;
	SESSION_ID = localStorage.getItem('SESSION_ID');
	// create user list (with and without team)
	setInterval(listSessionUsersWithoutTeam,1000)
	setInterval(listSessionUsersByTeam,1000)
	// check if session is starting
	setInterval(function(){checkSessionStatus(SESSION_ID); },1000);
	

	
	//add player to team (self), by clicking team header
    $('#team-list-wrapper').on('click', '.team-box-heading .team-icon', function () {
		localStorage.setItem('TEAM_ID', $(this).closest('[class~=team-box-wrapper]').attr('data-team-id')); 
		console.log('team chosen: you are in team ' + TEAM_ID);
		TEAM_ID = localStorage.getItem('TEAM_ID');	
		addPlayerToTeam(SESSION_ID, TEAM_ID, user.id);
	});
	

	// remove player from team, by clicking user remove icon
    $('#team-list-wrapper').on('click', '.kick-player-by-team-icon', function () {
		userId = $(this).closest('.team-user').attr('data-user-id');
		teamId = $(this).closest('[class~=team-box-wrapper]').attr('data-team-id');
		removePlayerFromTeam(SESSION_ID, teamId, userId);
	});
	
	// remove any player from session, by clicking user remove icon
    $('#players-without-team-list').on('click', '.kick-player-without-team-icon', function () {
		userId = $(this).closest('[class~=no-team-user]').attr('data-user-id');
		console.log('user removed from session: ' + userId);
		// host cant be kicked from session
		if(myself.role_id != 2) {
			removePlayerFromSession(SESSION_ID, userId);
		} else {
			console.log('HOST CANT BE KICKED');
		}	
	});
		
	// launch play session when button is clicked
	$('button#start-session').on('click',function () {
		// TODO remove session id from local storage and decode session obj
		// launch session
		launchSession(SESSION_ID);
	});
	
	function listSessionUsersWithoutTeam(){
	  var sessionId = JSON.parse(localStorage.getItem('SESSION_ID'));
	  var sessionToken = localStorage.getItem('SESSION_TOKEN');
	  $.ajax({
			 url: endpoint.sessionLobby + sessionId + '/users-without-team',
			 type: "GET",
			 beforeSend: function(request) {
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("Api-Token", TOKEN);
				//console.log("no team users: " + endpoint.sessionLobby + sessionId + '/users-without-team' + ' [GET]');
			 },
			 success: function(response) { 
			  console.log(response);
			  if(response.status=="success"){
				$('#players-without-team-list').empty();
				// add player without team to list
				$.each(response.data.users, function(index,users){
					if(users.user_id == user.id) {
						localStorage.setItem('TEAM_ID','null');
					}
					// add player to list
					SessionUsersWithoutTeamItem(users);
				});				
			  }
			 },
			  error: function(response) { 
			  console.log("error status : " + response.status);
			  console.log("error message : " + response.responseText);
			 }
		  });
	  }
	  
	  function SessionUsersWithoutTeamItem(users) {	 
		// create items
		var userNickname = null;
		if (myself.role_id == 2) {
			  userNickname = $('<h4></h4>').addClass('no-team-user-nickname').html(users.nickname + ' <i class="fa fa-times-circle kick-player-without-team-icon"></i>');
		  } else {
			  userNickname = $('<h4></h4>').addClass('no-team-user-nickname').html(users.nickname);
		  }
		var userNoTeamItem = $('<li></li>').addClass('col-12 col-sm-6 col-md-4 col-lg-3 centered no-team-user').attr('data-user-id',users.user_id).append(userNickname);
		$('#players-without-team-list').append(userNoTeamItem);
	  }
	    
	  function listSessionUsersByTeam(){
	  var sessionId = JSON.parse(localStorage.getItem('SESSION_ID'));
	  var sessionToken = localStorage.getItem('SESSION_TOKEN');
	  $.ajax({
			 url: endpoint.sessionLobby + sessionId,
			 type: "GET",
			 beforeSend: function(request) {
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("Api-Token", TOKEN);
				console.log("team users: " + endpoint.sessionLobby + sessionId + ' [GET]');
			 },
			 success: function(response) { 
			  console.log(response);
			  if(response.status=="success"){
				$('#team-list-wrapper').empty();
				// save sessions on local storage
				var sessionList = JSON.stringify(response);
				if (response.data.quizSession.user_id == myself.id) {
					myself.role_id = 2;
					$('button#start-session').show();
				}
				$.each(response.data.quizSession.teams, function(index,team){
					// create team panel
					SessionTeamsItem(team);
				});	
				// get team users
				$.each(response.data.quizSession.users, function(index,user){
					// save player in session to local storage
					if (user.user_id == myself.id) {
						myself.role_id = user.role_id;
						if (myself.role_id == 2) {
							$('button#start-session').show();
						}
					}
					// add player to team list
					SessionUsersByTeamItem(user);
				});				
			  }
			 },
			  error: function(response) { 
			  console.log("error status : " + response.status);
			  console.log("error message : " + response.responseText);
			 }
		  });
	  }
	  
	  function SessionTeamsItem(team) {	
		  TEAM_ID = localStorage.getItem('TEAM_ID');
		  // create team panel
		  var panel =		 $('<div></div>').addClass('col-12 col-md-6 team-box-wrapper').attr('data-team-id',team.id);
		  var panelHeading = $('<div></div>').addClass('team-box').append($('<div></div>').addClass('team-box-heading').attr('data-team-id',team.id)
																	.append($('<div></div>').addClass('row')
																		.append($('<div></div>').addClass('col-9')
																			.append($('<h3></h3>').text('TEAM '+team.name)))
																		.append($('<div></div>').addClass('col-3 centered team-icon-wrapper'))));
		  var panelBody =	 $('<div></div>').addClass('team-box-body');
		  var teamList = 	 $('<ul></ul>').addClass('list-group players-by-team-list'); 
		  $('#team-list-wrapper').append(panel.append(panelHeading).append(panelBody.append(teamList)));
		  console.log('TEAM_ID is: '+ TEAM_ID);
		  if (TEAM_ID == 'null') {
			$('.team-icon-wrapper').html('<i class="fa fa-2x fa-door-open team-icon"></i>');
		  } else {
			$('.team-icon-wrapper').empty();
		  }
		  $.each(team.users, function(index,user){
			// add player to team list
			teamList.append(SessionUsersByTeamItem(user, team.id));
		  });	  
	  }
	  
	  /*
					<div class="col-12 col-md-6">
						<!-- team box -->
						<div class="team-box">
							<div class="team-box-heading">
							  	<div class="row">
									<div class="col-10">
										<h3>TEAM BRAVO</h3>
									</div>
									<div class="col-2">
										<i class="fa fa-2x fa-door-open team-icon"></i>
									</div>
								</div>
							</div>
							<div class="team-box-body">
								<!-- multiplayer player without team list -->
								<ul class="list-group" id="players-by-team-list">
									<li class="">
									  <div class="row">
										 <div class="col-10">
										  ProvaNomeUtente
										 </div>
										 <div class="col-2">
										   <i class="fa fa-check player-by-team-icon"></i>
										 </div>
									  </div>
									</li>
									<li class="">
									  <div class="row">
										 <div class="col-10">
										  CiSonoAncheIo
										 </div>
										 <div class="col-2">
										   <i class="fa fa-check player-by-team-icon"></i>
										 </div>
									  </div>
									</li>
								</ul>
								<!-- end multiplayer player without team list -->
							</div>
						</div>
	  */
	  
	  function SessionUsersByTeamItem(user, teamId) {	 
		  // create bootstrap cols
		  var teamListItem = $('<li></li>').addClass('team-user').attr('data-user-id', user.user_id);
		  var teamListFirstCol = null;
		  if (myself.role_id == 2  || myself.id == user.user_id) {
			  teamListFirstCol = $('<div></div>').addClass('col-10').append($('<h4></h4>').addClass('team-user-nickname').html(user.nickname + ' <i class="fa fa-times-circle kick-player-by-team-icon"></i>'));
		  } else {
			  teamListFirstCol = $('<div></div>').addClass('col-10').append($('<h4></h4>').addClass('team-user-nickname').html(user.nickname));
		  }
		  var teamListSecondCol = $('<div></div>').addClass('col-2').html('<i class="fa fa-check fa-2x player-by-team-icon"></i>');
		  teamListItem = teamListItem.append($('<div></div>').addClass('row').append(teamListFirstCol).append(teamListSecondCol));
		  if (user.id == myself.id) {
			  myself.team_id = teamId;
			  localStorage.setItem('TEAM_ID',teamId);
		  }
		  		  
		  return teamListItem;
	  }
	  
	  /*
				<li class="">
				  <div class="row">
					 <div class="col-10">
					  ProvaNomeUtente
					 </div>
					 <div class="col-2">
					   <i class="fa fa-check player-by-team-icon"></i>
					 </div>
				  </div>
				</li>
	  */
	  
	  function launchSession(sessionId){
		// get user id from local storage
		var userId = localStorage.getItem('USER_ID');
		$.ajax({
			 url: endpoint.sessionLaunch,
 			 type: "POST",
			 beforeSend: function(request) {
				console.log('calling launch function: ' + endpoint.sessionLaunch);
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
			 },
			 data: {
				 user_id			: userId,
				 quiz_session_id	: sessionId
			 },
			 success: function(response) { 
			 console.log(response);
			  if(response.status=="success"){
				// redirect to play page
				window.location.href = "play.html";
			  }
			 },
			 error: function(jqXHR, exception) {
				console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
		}
		
		function addPlayerToTeam(sessionId, teamId, userId){
		// get user id from local storage
		$.ajax({
			 url: endpoint.sessionLobby + sessionId + '/teams/' + teamId + '/users',
 			 type: "POST",
			 beforeSend: function(request) {
				console.log('calling player to team function: ' + endpoint.sessionLobby + sessionId + '/teams/' + teamId + '/users');
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
			 },
			 data: {
				 user_id			: userId
			 },
			 success: function(response) { 
			 console.log(response);
			  if(response.status=="success"){
				console.log(response.errors.message);
				if(userId == user.id) {
					localStorage.setItem('TEAM_ID', teamId);
					myself.team_id = teamId;
				}
			  } else if (response.status=="fail") {
				  console.log(response);
			  }
			 },
			 error: function(jqXHR, exception, response) {
				console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
		}
		
		function removePlayerFromTeam(sessionId, teamId, userId){
		$.ajax({
			 url: endpoint.sessionLobby + sessionId + '/teams/' + teamId + '/users/' + userId,
 			 type: "DELETE",
			 beforeSend: function(request) {
				console.log('remove player from team: ' + endpoint.sessionLobby + sessionId + '/teams/' + teamId + '/users/' + userId + '[DELETE]');
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
			 },
			 success: function(response) { 
			 console.log(response);
			  if(response.status=="success"){
				console.log(response.errors.message);
				console.log('removed from team: ' + teamId);
				console.log('user removed: ' + userId);
				console.log('user.id: ' + user.id);
				console.log('type of userId: ' + typeof(userId));
				console.log('type of user.id: ' + typeof(user.id));
				if (userId == user.id) {
					localStorage.setItem('TEAM_ID','null');
					myself.team_id = null;
				}
			  } else if (response.status=="fail") {
				  console.log(response);
			  }
			 },
			 error: function(jqXHR, exception, response) {
				console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
		}
		
		function removePlayerFromSession(sessionId, userId){
		$.ajax({
			 url: endpoint.sessionLobby + sessionId + '/users/' + userId,
 			 type: "DELETE",
			 beforeSend: function(request) {
				console.log('remove player from session: ' + endpoint.sessionLobby + sessionId + '/users/' + userId + '[DELETE]');
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
			 },
			 success: function(response) { 
			 console.log(response);
			  if(response.status=="success"){
				console.log(response.errors.message);
				localStorage.removeItem('USER_IN_SESSION');
			  } else if (response.status=="fail") {
				  console.log(response);
			  }
			 },
			 error: function(jqXHR, exception, response) {
				console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
		}
		
		function checkSessionStatus(sessionId){
		// get user id from local storage
		$.ajax({
			 url: endpoint.sessionLobby + sessionId,
 			 type: "GET",
			 beforeSend: function(request) {
				console.log('checking session: ' + endpoint.sessionLobby + sessionId + '[GET]');
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
			 },
			 success: function(response) { 
			  // if session has been launched, redirect to play view
			  if(response.data.quizSession.quiz_session_status_id == 3){
				window.location.href = "play.html";
			  }
			 },
			 error: function(jqXHR, exception, response) {
				console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
		}
	
// END document.ready
});	
	

