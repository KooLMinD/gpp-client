  $(document).ready(function () {
	  
	if (getCookie('tutorial-trophy') !== 'unlocked') {
		$('#tutorial-modal').modal('show');  
		
		$('#tutorial-modal').on('hidden.bs.modal', function (e) {
			
		  if (getCookie('login-trophy') !== 'unlocked') {
				$.notify(
				{
					icon: 'fa fa-home',
					title: 'Benvenuto a casa!',
					message: 'Registrati ed effettua il primo login'
				}, {
					type: 'none',
					onShow: trophyAudio()
				});
			
			setCookie('login-trophy', 'unlocked', 2);
			};
			
		  if (getCookie('tutorial-trophy') !== 'unlocked') {	
			  $.notify(
				{
					icon: 'fa fa-sort-alpha-asc',
					title: 'Non puoi impararmi niente, sono insegnato',
					message: 'Completa il tour introduttivo.'
				}, {
					type: 'none',
					onShow: trophyAudio()
				});
				
				setCookie('tutorial-trophy', 'unlocked', 2);
			};
	    });
			
	};

  });
  
  