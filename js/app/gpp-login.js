/* Author			: Roberto Taglialatela, Alessandro Fioroni
   Creation date	: 20.03.2018
   Last updated		: 04.05.2018
   Content			: login function
*/

	function gppLogin(){
		$.ajax({
			 url: endpoint.login,
 			 type: "POST",
			 beforeSend: function(request) {
				console.log(endpoint.login);
				request.setRequestHeader("X-localization", LANG);
			 },
			 data: 
			 { 
			   email: $('input[name=login-email]').val(),
			   password: $('input[name=login-password]').val()
			 },
			 success: function(response) { 
			 console.log(response);
			  if(response.status=="success"){
				console.log(response.message);
				//Storing API token for this session
				TOKEN = response.data.api_token;
				localStorage.setItem('TOKEN',TOKEN);
				// add player to local storage
				var user = JSON.stringify(response.data.user)
				localStorage.setItem('USER', user);
				console.log('user stored is: ');
				console.log(user);
				user = JSON.parse(localStorage.getItem('USER'));
				console.log('decoded user id: ' + user.id);
				// redirect to quiz lobby
				window.location.href = "lobby.html";
			  }
			 },
			 error: function(jqXHR, exception) {
				console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
		}
	  
$( document ).ready(function() {
	/* if user is already logged in, redirect to home page
	console.log(TOKEN);
	if (localStorage.getItem('TOKEN') != null) {
		window.location.href = "lobby.html";
	}
	*/
	//login button listener
	$( '#login-bt' ).on( "click", gppLogin);	
	
});	
	