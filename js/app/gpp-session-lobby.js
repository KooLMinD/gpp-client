/* Author			: Roberto Taglialatela
   Creation date	: 29.04.2018
   Last updated		: 04.05.2018
   Content			: manage session lobby before playing
*/

$(document).ready(function() {
	/*-----------------------------------------------------------------------------------------------*/
	/*------------------------------------   SESSION LOBBY.HTML   -----------------------------------*/
	/*-----------------------------------------------------------------------------------------------*/
	
	// create user list
	listSessionUsers();
	
	/* change color when clicking start session button */
	$('button#start-session').on('click',function () {
		var sessionId = localStorage.getItem('SESSION_ID');
		// TODO remove sesssion id from local storage and decode session obj
		// var session = JSON.parse(localStorage.getItem('SESSION_OBJ');
		// var sessionId = session.id;
		if (1) {
			// launch session
			launchSession(sessionId);
		}
		// TODO change this to :active CSS
		$(this).toggleClass('clicked');
	});
	
	function listSessionUsers(){
	  var sessionId = JSON.parse(localStorage.getItem('SESSION_ID'));
	  var sessionToken = localStorage.getItem('SESSION_TOKEN');
	  $.ajax({
			 url: endpoint.sessionLobby + sessionId,
			 type: "GET",
			 beforeSend: function(request) {
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("Api-Token", TOKEN);
				console.log("url called: " + endpoint.sessionLobby + sessionId + ' [GET]');
				console.log("SESSION_TOKEN IS: " + sessionToken);
			 },
			 success: function(response) { 
			  console.log(response);
			  if(response.status=="success"){
				// save sessions on local storage
				var sessionList = JSON.stringify(response);
				localStorage.setItem('SESSION_LIST', sessionList);
				//Cycling through users one by one
				$.each(response.data.quizSession.users, function(index,user){
					// add player to list
					createSessionUserItem(user.nickname);
				});				
			  }
			 },
			  error: function(response) { 
			  console.log("error status : " + response.status);
			  console.log("error message : " + response.responseText);
			 }
		  });
	  }
	  
	  function createSessionUserItem(nickname) {	 
		  // create bootstrap cols
		  var firstCol = $('<div></div>').addClass('col-xs-8 col-sm-10').append($('<h4></h4>').addClass('text-subhead').text(nickname));
		  var secondCol = $('<div></div>').addClass('col-xs-4 col-sm-2').append($('<h4></h4>').addClass('text-subhead text-green-800 text-right').html('<i class="fa fa-check-square fa-2x"></i>'));
		 $('#session-user-list').append($('<li></li>').addClass('list-group-item media v-middle').append(firstCol).append(secondCol));
	  }
	  
	  function launchSession(sessionId){
		// get user id from local storage
		var userId = localStorage.getItem('USER_ID');
		$.ajax({
			 url: endpoint.sessionLaunch,
 			 type: "POST",
			 beforeSend: function(request) {
				console.log('calling launch function: ' + endpoint.sessionLaunch);
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
			 },
			 data: {
				 user_id			: userId,
				 quiz_session_id	: sessionId
			 },
			 success: function(response) { 
			 console.log(response);
			  if(response.status=="success"){
			  //if(1){
				// redirect to play page
				window.location.href = "play.html";
			  }
			 },
			 error: function(jqXHR, exception) {
				console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
		}
		
	
// END document.ready
});	
	

