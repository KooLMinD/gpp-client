/* Author			: Roberto Taglialatela
   Creation date	: 29.06.2018
   Last updated		: 21.08.2018
   Content			: manage quiz list
*/

$(document).ready(function() {
		
	/*-----------------------------------------------------------------------------------------------*/
	/*------------------------------------   QUIZ-LIST.HTML   --------------------------------------------*/
	/*-----------------------------------------------------------------------------------------------*/

	// get session id from local storage
	TOKEN 				= localStorage.getItem('TOKEN');
	var user 			= JSON.parse(localStorage.getItem('USER'));
	var userId			= user.id;
	var quizId			= null;
	var hostRole		= 0;
	
	getQuizList();
	
	/* launch edit quiz funcion when edit button is clicked */
	$('#quiz-list').on('click','.quiz-item .edit',function () {
		quizId = $(this).closest('.quiz-item').attr('data-quiz-id');
		localStorage.setItem('UPDATE_QUIZ_ID', quizId);
		console.log('editing quiz: ' + quizId);
		window.location.href = "edit-quiz.html";
	});
	
	/* launch delete quiz funcion when delete button is clicked */
	$('#quiz-list').on('click','.quiz-item .delete',function () {
		deletedQuiz = $(this).closest('.quiz-item');
		quizId = $(this).closest('.quiz-item').attr('data-quiz-id');
		$(deletedQuiz).fadeOut(3000);
		showAlert('info','cancellazione in corso. <button class="undoDelete btn btn-info" data-quiz-id="'+quizId+'" data-timeout-id="'+timerIndex+'">Annulla operazione</button>');
		timers[timerIndex] = setTimeout(function() {deleteQuiz(quizId)}, 5000);
		console.log('timer index is '+timerIndex);
		++timerIndex;
	});
	
	// undo delete action
	$('body').on('click', '.undoDelete',function() {
		var deleteQuizId = $(this).attr('data-quiz-id');
		var timeoutId = $(this).attr('data-timeout-id');
		console.log('timeout ID is '+timeoutId);
		showAlert('success','cancellazione del quiz '+deleteQuizId+' annullata. timer index '+timeoutId,2000);
		clearTimeout(timers[timeoutId]);
		$('.quiz-item[data-quiz-id='+deleteQuizId+']').stop().fadeIn(1000);	
		$(this).closest('.alert').remove();
	});
	
	/* launch clone quiz function when clone button is clicked */
	$('#quiz-list').on('click','.quiz-item .duplicate',function () {
		quizId = $(this).closest('.quiz-item').attr('data-quiz-id');
		console.log('duplicating quiz: ' + quizId);
		showAlert('success','quiz duplicato :)');
		getQuiz(quizId);
	});
	
	/* Open modal for host role when play button is clicked */
	$('#quiz-list').on('click','.quiz-item .play',function () {
		quizId = $(this).closest('.quiz-item').attr('data-quiz-id');
		console.log('playing quiz: ' + quizId);
	});
	
	/* get host role and launch session*/
	$('#host-role').on('click','.playing-host',function () {
		hostRole = 1;
		console.log('host role is: ' + hostRole);
		createNewSession(userId, quizId, hostRole);
	});
	$('#host-role').on('click','.host',function () {
		console.log('host role is: ' + hostRole);
		createNewSession(userId, quizId, hostRole);
	});
	
	
	function getQuizList(){	
		$.ajax({
			 url: endpoint.getQuizzes,
			 type: "GET",
			 beforeSend: function(request) {
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
				console.log("url called: " + endpoint.getQuizzes + ' [GET]');
			 },
			 success: function(response) { 
			    // reset quiz list
			    $('#quiz-list').empty();
				console.log(response);
				// create quiz box for each existing quiz
				$.each(response.data.quizzes, function(index,quiz){
					createQuizItem(quiz, index);
				});	
			 },
			 error: function(jqXHR, exception) {
				console.log("got errors: " + jqXHR.message);
				console.log(jqXHR.status);
			 }
		});
	}
	
	function deleteQuiz(quizId){
		$.ajax({
			 url: endpoint.deleteQuiz + quizId,
			 type: "DELETE",
			 beforeSend: function(request) {
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
				console.log("url called: " + endpoint.deleteQuiz + quizId + ' [DELETE]');
			 },
			 success: function(response) { 
			    console.log('delete successful');
				console.log(response);
				getQuizList();
			 },
			 error: function(jqXHR, exception) {
				console.log("got errors: " + jqXHR.message);
				console.log(jqXHR.status);
			 }
		  });
	}
		
	function getQuiz(quizId){
		// get quiz data to duplicate it
		$.ajax({
				 url: endpoint.getQuiz + quizId,
				 type: "GET",
				 beforeSend: function(request) {
					TOKEN = localStorage.getItem('TOKEN');
					request.setRequestHeader("X-localization", LANG);
					request.setRequestHeader("Api-Token", TOKEN);
					console.log("url called: " + endpoint.getQuiz + quizId, + ' [GET]');
				 },
				 success: function(response) { 
				  	console.log(response);
					// launch duplicate function for selected quiz
					duplicateQuiz(response);
				 },
				 error: function(jqXHR, exception) {
					console.log("got errors: " + jqXHR.message);
					console.log(jqXHR.status);
				 }
			  });
	}
	
	function duplicateQuiz(quizObj){
		// launch quiz creation for duplicated quiz
		$.ajax({
				 url: endpoint.createQuiz,
				 type: "POST",
				 beforeSend: function(request) {
					TOKEN = localStorage.getItem('TOKEN');
					request.setRequestHeader("X-localization", LANG);
					request.setRequestHeader("Api-Token", TOKEN);
					console.log("url called: " + endpoint.createQuiz + ' [POST]');
					console.log(quizObj);
				 },
				 data: {
					 user_id			: userId,
					 title				: '[COPIA] ' + quizObj.data.quiz.title,
					 description		: quizObj.data.quiz.description,
					 private			: quizObj.data.quiz.private,
					 language_id		: quizObj.data.quiz.language_id,
					 questions			: quizObj.data.quiz.questions
				 },
				 success: function(response) { 
				  	console.log(response);
					//if quiz is saved, create new quiz item for the quiz list
					createQuizItem(response.data.quiz);
				 },
				 error: function(jqXHR, exception) {
					console.log("got errors: " + jqXHR.message);
					console.log(jqXHR.status);
				 }
			  });
		}
			
	function createQuizItem(quiz, index) {	 
		  // create existing quiz list item
		  var playButton		= $('<div></div>').addClass('col-6 col-sm-2 col-md-1 text-center play').append($('<div></div>').addClass('button-circle-wrapper').attr({role: 'button', 'data-toggle': 'modal', 'data-target': '#host-role'}).append($('<div></div>').addClass('button-circle').append($('<i></i>').addClass('fa fa-play fa-2x'))));
		  var editButton		= $('<div></div>').addClass('col-6 col-sm-2 col-md-1 text-center edit').append($('<div></div>').addClass('button-circle-wrapper').attr({role:'button'}).append($('<div></div>').addClass('button-circle').append($('<i></i>').addClass('fa fa-edit fa-2x'))));
		  var duplicateButton	= $('<div></div>').addClass('col-6 col-sm-2 col-md-1 text-center duplicate').append($('<div></div>').addClass('button-circle-wrapper').attr({role:'button'}).append($('<div></div>').addClass('button-circle').append($('<i></i>').addClass('fa fa-copy fa-2x'))));
		  var deleteButton		= $('<div></div>').addClass('col-6 col-sm-2 col-md-1 text-center delete').append($('<div></div>').addClass('button-circle-wrapper').attr({role:'button'}).append($('<div></div>').addClass('button-circle').append($('<i></i>').addClass('fa fa-times fa-2x'))));
		  var quizBody			= $('<div></div>').addClass('col-md-12').append($('<h4></h4>').text(quiz.title));
		  var quizItem 			= $('<li></li>').addClass('quiz-item').attr('data-quiz-id',	quiz.id).css({'display':'none'})
												.append($('<div></div>').addClass('container')
													.append($('<div></div>').addClass('row')
														.append($('<div></div>').addClass('col-md-12 content')
															.append($('<div></div>').addClass('container')
																.append($('<div></div>').addClass('row')
																	.append(quizBody)))))
													.append($('<div></div>').addClass('row justify-content-center quiz-manage-buttons')
														.append(playButton)
														.append(editButton)
														.append(duplicateButton)
														.append(deleteButton)));
		  $('#quiz-list').append($(quizItem).fadeIn(1000));
		}
		
	function createNewSession(userId, quizId, hostRole){
		$.ajax({
			 url: endpoint.createSession,
 			 type: "POST",
			 beforeSend: function(request) {
				console.log('calling create new session URL: ' + endpoint.createSession);
				console.log('quiz id: ' + quizId);
				console.log('user id: ' + userId);
				request.setRequestHeader("Api-Token", TOKEN);
				request.setRequestHeader("X-localization", LANG);
			 },
			 data: {
				 user_id: userId,
				 quiz_id: quizId, 
				 playing_host: hostRole
			 },
			 success: function(response) { 
			 console.log(response);
			  if(response.status=="success"){
				//Storing SESSION_OBJ for this session
				SESSION_OBJ = JSON.stringify(response.data.quizSession);
				localStorage.setItem('SESSION_OBJ', SESSION_OBJ);
				console.log("SESSION_OBJ STORED: " + SESSION_OBJ);
				// TODO remove session id and decode from session OBJ
				//Storing SESSION_ID for this session
				SESSION_ID = response.data.quizSession.id;
				localStorage.setItem('SESSION_ID', SESSION_ID);
				console.log("SESSION_ID STORED: " + SESSION_ID);
				//Storing SESSION_TOKEN for this session
				SESSION_TOKEN = response.data.quizSession.token;
				localStorage.setItem('SESSION_TOKEN', SESSION_TOKEN);
				console.log("SESSION_TOKEN STORED: " + SESSION_TOKEN);
				// redirect to session lobby
				window.location.href = "session-lobby-team.html";
			  }
			  if(response.status=="fail"){
				// show fail message
				console.log(response.message);
			  }
			 },
			 error: function(jqXHR, response, exception) {
				console.log(response);
				console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
		}
	    
	  
	//TODO after adding new question, scoll to bottom and set focus on question input
  
// END document.ready
});	
	
