/* 
   Author			: Roberto Taglialatela
   Creation date	: 20.03.2018
   Last updated		: 04.05.2018
   Content			: manage session list (create, search, join player)
*/

$(document).ready(function() {
	/*-----------------------------------------------------------------------------------------------*/
	/*------------------------------------   LOBBY.HTML   -------------------------------------------*/
	/*-----------------------------------------------------------------------------------------------*/
	
	/* TESTING ONLY reset sessions */
    $('#reset-sessions').on('click', function () {
		resetSessions();
	});
	
	// TESTING ONLY reset sessions and create one for testing
	function resetSessions(){
	// get user id from local storage
	$.ajax({
		 url: endpoint.resetSessions,
		 type: "GET",
		 beforeSend: function(request) {
			console.log('resetting session: ' + endpoint.resetSessions + '[GET]');
			TOKEN = localStorage.getItem('TOKEN');
			request.setRequestHeader("X-localization", LANG);
			request.setRequestHeader("Api-Token", TOKEN);
		 },
		 success: function(response) { 
		  //console.log(response);
		  console.log('session reset succesfully');
		  //window.location.href = "lobby.html";
		 },
		 error: function(jqXHR, exception, response) {
			console.log("error status : " + response.status);
			console.log("error message : " + response.responseText);
		 }
	  });
	}

	//launch session list on page load
	var sessionListTimer = setInterval(listQuizSessions,1000);

	// on user clicking join session, add player and redirect to session lobby
	$('#quiz-session-list').on( "click", ".join-session", function(){
		var sessionId = $(this).attr('data-session-id');
		var user = JSON.parse(localStorage.getItem('USER'));
		var userId = user.id;
		localStorage.setItem('SESSION_ID', sessionId);
		// if user is already in session, just redicted to session lobby, if not add player to chosen session
		setTimeout(checkPlayerInSession(userId, sessionId),500);
	});
	
	// on user creating new session, add session and redirect to session lobby
	$('#create-session-form').on( "click", "#create-session-btn", function(){
		var quizId = $('#quiz-id').val();
		var user = JSON.parse(localStorage.getItem('USER'));
		var userId = user.id;
		console.log('GOT quiz id from select: ' + quizId);
		console.log('DECODED user id from local storage: ' + userId);
		createNewSession(userId, quizId);
	});
	
	
	function listQuizSessions(){
	  $.ajax({
			 url: endpoint.sessionList,
			 type: "GET",
			 beforeSend: function(request) {
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("Api-Token", TOKEN);
				console.log("url called: " + endpoint.sessionList + '[GET]');
			 },
			 success: function(response) { 
			  console.log(response);
			  if(response.status=="success"){
				// destroy previous list items
				$('#quiz-session-list .session').remove();
				// save sessions on local storage
				var sessionList = JSON.stringify(response);
				localStorage.setItem('SESSIONS_LIST', sessionList);
				//Quiz sessions loaded - Create open session list
				$.each(response.data.quizSessions, function(index,session){
					if (session.quiz_session_status.id == 1) {
						createSessionItem(session);
					}				  
				});				
			  }
			 },
			  error: function(response) { 
			    console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
	  }
	  
	  function createSessionItem(session) {
		  console.log(session);
		  //count joined players total
		  var joinedPlayers = session.users.length;
		  var sessionId = session.id;
		  var quizTitle = session.quiz.title
		  // create bootstrap cols
		  var firstCol = $('<div></div>').addClass('col-xs-4 col-sm-2').append($('<span></span>').text(session.id));
		  var secondCol = $('<div></div>').addClass('col-xs-7 col-sm-6').append($('<span></span>').text(quizTitle));
		  var thirdCol = $('<div></div>').addClass('col-xs-2 col-sm-2').append($('<span></span>').text(joinedPlayers));
		  var fourthCol = $('<div></div>').addClass('col-xs-12 col-sm-2').append($('<div></div>').addClass('media-slot text-right').append($('<button></button>').addClass('btn btn-primary join-session').attr('data-session-id',session.id).text('accedi')));
		  $('#quiz-session-list').append($('<li></li>').addClass('list-responsive-item row session').append(firstCol).append(secondCol).append(thirdCol).append(fourthCol));
	  }
	  
	  /*
			<li class="list-responsive-item row">
			  <div class="col-xs-12	col-sm-2">
				<span class="">432325789</span>	
			  </div>
			  <div class="col-xs-12 col-sm-8">
				<span class="">Quiz for testing purposes</span>								
			  </div>
			  <div class="col-xs-12 col-sm-2">
				<span class="">5</span>		
			  </div>
			</li>
	  */
	  
	  function addPlayerToSession(userId, sessionId){
		$.ajax({
			 url: endpoint.sessionLobby + sessionId + '/users',
 			 type: "POST",
			 beforeSend: function(request) {
				console.log('calling add player url: ' + endpoint.sessionLobby + sessionId + '/users');
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("X-localization", LANG);
				request.setRequestHeader("Api-Token", TOKEN);
			 },
			 data: {
				 user_id: userId
			 },
			 success: function(response) { 
			 console.log(response);
			  if(response.status=="success"){
				console.log('player added to session');
				//Storing SESSION_OBJ for this session
				SESSION_OBJ = JSON.stringify(response.data.quizSession);
				localStorage.setItem('SESSION_OBJ', SESSION_OBJ);
				console.log("SESSION_OBJ STORED: " + SESSION_OBJ);
				// TODO remove session id and decode from session OBJ
				//Storing SESSION_ID for this session
				SESSION_ID = response.data.quizSession.id;
				localStorage.setItem('SESSION_ID', SESSION_ID);
				//Storing SESSION_TOKEN for this session
				SESSION_TOKEN = response.data.quizSession.token;
				localStorage.setItem('SESSION_TOKEN', SESSION_TOKEN);
				// redirect to session lobby
				window.location.href = "session-lobby-team.html";
			  }
			  if (response.status == "fail") {
				  console.log(response);
			  }
			 },
			 error: function(jqXHR, response, exception) {
				console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
		}
		
		function createNewSession(userId, quizId){
		$.ajax({
			 url: endpoint.createSession,
 			 type: "POST",
			 beforeSend: function(request) {
				console.log('calling create new session URL: ' + endpoint.createSession);
				console.log('quiz id: ' + quizId);
				console.log('user id: ' + userId);
				request.setRequestHeader("Api-Token", TOKEN);
				request.setRequestHeader("X-localization", LANG);
			 },
			 data: {
				 user_id: userId,
				 quiz_id: quizId
			 },
			 success: function(response) { 
			 console.log(response);
			  if(response.status=="success"){
				//Storing SESSION_OBJ for this session
				SESSION_OBJ = JSON.stringify(response.data.quizSession);
				localStorage.setItem('SESSION_OBJ', SESSION_OBJ);
				console.log("SESSION_OBJ STORED: " + SESSION_OBJ);
				// TODO remove session id and decode from session OBJ
				//Storing SESSION_ID for this session
				SESSION_ID = response.data.quizSession.id;
				localStorage.setItem('SESSION_ID', SESSION_ID);
				console.log("SESSION_ID STORED: " + SESSION_ID);
				//Storing SESSION_TOKEN for this session
				SESSION_TOKEN = response.data.quizSession.token;
				localStorage.setItem('SESSION_TOKEN', SESSION_TOKEN);
				console.log("SESSION_TOKEN STORED: " + SESSION_TOKEN);
				// redirect to session lobby
				window.location.href = "session-lobby-team.html";
			  }
			  if(response.status=="fail"){
				// show fail message
				console.log(response.message);
			  }
			 },
			 error: function(jqXHR, response, exception) {
				console.log(response);
				console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
		}
		
	  function checkPlayerInSession(userId, sessionId){
	    $.ajax({
			 url: endpoint.sessionLobby + sessionId,
			 type: "GET",
			 beforeSend: function(request) {
				TOKEN = localStorage.getItem('TOKEN');
				request.setRequestHeader("Api-Token", TOKEN);
				console.log("checking user " + userId + " in session: " + endpoint.sessionLobby + sessionId + '[GET]');
			 },
			 success: function(response) { 
			  if(response.status=="success"){
				var playerInSession = false;
				// check if player is already in this session
				$.each(response.data.quizSession.users, function(index,user){
					if (user.user_id == userId) {
						playerInSession = true;
					}				
				});
			    console.log('player in session after check: ' + playerInSession);
				if(playerInSession) {
					console.log('you are in session, should redirect');
					window.location.href = "session-lobby-team.html";	
				} else if (playerInSession == false){
					console.log('you are NOT in session, should add and redirect');
					addPlayerToSession(userId, sessionId);
				} else {
					console.log('error checking player');
				}
			  }
			 },
			  error: function(response) { 
			    console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
	  }
		
// END document.ready
});	

