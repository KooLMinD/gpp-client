/* Author			: Roberto Taglialatela, Alessandro Fioroni
   Creation date	: 20.03.2018
   Last updated		: 15.08.2018
   Content			: gpp global configuration
*/

	// GLOBALS
	timers = [];
	timerIndex = 0;
	
	//COSTANTS
	var APIBASE 		= 'http://localhost/gpp-core/public/api/v1';
	var TOKEN 			= null;
	var SESSION_TOKEN	= null;
	var SESSIONS_LIST	= null;
	var SESSION_ID		= null;
	var LANG 			= 'it';
	var QUIZ_ID 		= null;
	var QUIZ_SESSION 	= null;
	var TEAM_ID			= null;
	var USER_ID			= null;
	var UPDATE_QUIZ_ID	= null;

	//ENDPOINTS
	var endpoint = {
		login 			: APIBASE + "/login" ,
		logout			: APIBASE + "/logout" ,
		register		: APIBASE + "/register" , 
		users : {
			list		: APIBASE + "/users"
		},
		getQuizzes		: APIBASE + "/quizzes",
		getQuiz			: APIBASE + "/quizzes/", 
		createQuiz		: APIBASE + "/quizzes",
		editQuiz		: APIBASE + "/quizzes/", 
		deleteQuiz		: APIBASE + "/quizzes/", 
		createSession	: APIBASE + "/quiz-session",
		selectedQuiz	: APIBASE + "/quizzes/",
		sessionList		: APIBASE + "/quiz-session",
		sessionLobby	: APIBASE + "/quiz-session/",
		sessionLaunch	: APIBASE + "/quiz-session/launch",
		resetSessions	: APIBASE + "/testing/reset-sessions",
		createTrophy	: APIBASE + "/trophies",
		getTrophies		: APIBASE + "/users/",
		trophiesAchieved: APIBASE + "/trophies/session/user",
		updateProfile	: APIBASE + "/users/"
	};
	
	// COMMON FUNCTIONS
	$(document).ready(function() {
		$('#gpp-logout').on('click', gppLogout);
		
		$('#new-quiz-btn').on('click', function () {
			window.location.href = "new-quiz.html";
		});	
	});
		
	// js validation: disable form submissions if there are invalid fields
	(function() {
		  'use strict';
		  window.addEventListener('load', function() {
			// Fetch all the forms we want to apply custom Bootstrap validation styles to
			var forms = document.getElementsByClassName('needs-validation');
			// Loop over them and prevent submission
			var validation = Array.prototype.filter.call(forms, function(form) {
			  form.addEventListener('submit', function(event) {
				if (form.checkValidity() === false) {
				  event.stopPropagation();
				}
				event.preventDefault();
				form.classList.add('was-validated');
				$("#"+document.querySelectorAll(":invalid")[1].id).focus();
				$(window).scrollTop($("#"+document.querySelectorAll(":invalid")[1].id).position().top);
			  }, false);
			});
		  }, false);
		})();
		
			
    function showAlert(type, message, delay) {
		var delayTime = null;
		var defaultDelay = 4000;
		delay ? delayTime=delay : delayTime=defaultDelay;
		var alertWrapper = $('<div></div>').attr('id','alert-wrapper');
		var alertItem = $('<div></div>').addClass('alert-item alert');
		var alertContent = $('<div></div>').append($('<h4></h4>'));
		$('body').append(alertWrapper);

		if (type == 'success') {
			$(alertItem).addClass('alert-success');
			$(alertContent).children('h4:first').html('<i class="fas fa-check-circle"></i> ' + message);
			$('#alert-wrapper').append($(alertItem).append($(alertContent)));
			$('.alert-item:last-of-type').slideDown(500).delay(delayTime).slideUp(400, function(){$(this).remove()});
		} else if (type == 'warning') {
			$(alertItem).addClass('alert-warning');
			$(alertContent).children('h4:first').html('<i class="fas fa-exclamation-circle"></i> ' + message);
			$('#alert-wrapper').append($(alertItem).append($(alertContent)));
			$('.alert-item:last-of-type').slideDown(500).delay(delayTime).slideUp(400, function(){$(this).remove()});
		} else if (type == 'danger') {
			$(alertItem).addClass('alert-danger');
			$(alertContent).children('h4:first').html('<i class="fas fa-times-circle"></i> ' + message);
			$('#alert-wrapper').append($(alertItem).append($(alertContent)));
			$('.alert-item:last-of-type').slideDown(500).delay(delayTime).slideUp(400, function(){$(this).remove()});
		} else if (type == 'info') {
			$(alertItem).addClass('alert-info');
			$(alertContent).children('h4:first').html('<i class="fas fa-info-circle"></i> ' + message);
			$('#alert-wrapper').append($(alertItem).append($(alertContent)));
			$('.alert-item:last-of-type').slideDown(500).delay(delayTime).slideUp(400, function(){$(this).remove()});
		}
	}
	
		function gppLogout(){
		$.ajax({
			 url: endpoint.logout,
 			 type: "POST",
			 beforeSend: function(request) {
				console.log('calling logout: ' + endpoint.logout + ' [POST]');
				request.setRequestHeader("Api-Token", TOKEN);
				request.setRequestHeader("X-localization", LANG);
			 },
			 success: function(response) { 
			  console.log(response);
			  if(response.status=="success"){
				// if logout is successful, clear local storage and redirect to home page
				console.log('logout successful');
				localStorage.clear();
				// redirect to home page
				window.location.href = "index.html";
			  }
			 },
			 error: function(jqXHR, exception, response) {
				console.log(response);
				console.log("error status : " + response.status);
			    console.log("error message : " + response.responseText);
			 }
		  });
		}
		
