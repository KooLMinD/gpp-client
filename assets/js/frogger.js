/*
 Neat Programming Trick: animation in the console
 For extra geekiness appreciation, open your browser's console (command+option+j on mac/chrome)
 By @jschomay
 */

/* noprotect */
function frogger() {
    var animate, checkCollision, drawCycles, drawFrog, frogChar, frogPos, goal, keypressed, laneTemplate, lose, movements, moves, paused, start, street, tries, win;

    laneTemplate = "<0>-----------------------<0>--------------";
    start = goal = "ooooooooooooooooooooooooooooooooooooooooooo";
    frogChar = 'X';
    frogPos = [0, 20];
    street = [];
    paused = false;
    drawCycles = 0;
    moves = 0;
    tries = 1;

    drawFrog = function (frogPos) {
        var laneCharUnderFrog;
        laneCharUnderFrog = street[frogPos[0]].substr(frogPos[1] + 1, 1);
        street[frogPos[0]] = street[frogPos[0]].substr(0, frogPos[1]) + frogChar + street[frogPos[0]].substr(frogPos[1] + frogChar.length);
        return laneCharUnderFrog;
    };

    checkCollision = function (laneCharUnderFrog) {
        return /[<0>]/.test(laneCharUnderFrog);
    };

    lose = function () {
        paused = true;
        return setTimeout(function () {
            console.log('Game Over');
            paused = true;
            frogPos = [0, 20];
            return tries++;
        }, 60);
    };

    win = function () {
        paused = true;
        return setTimeout(function () {
//            alert("GREAT!  You made it!  Nice.\n\nIt took you " + tries + " tries, " + moves + " moves, and " + drawCycles + " draw cycles");
            $.notify({
                icon: 'fa fa-car',
                title: 'Prima di attraversare...',
                message: 'Hai scoperto e battuto frogger!'
            }, {
                type: 'none',
				onShow: trophyAudio()
            });
            paused = true;
            moves = drawCycles = 0;
            tries = 1;
            return frogPos = [0, 20];
        }, 60);
    };

    keypressed = null;

    movements = {
        38: function () {
            if (frogPos[0] < street.length - 1) {
                return frogPos[0]++;
            }
        },
        40: function () {
            if (frogPos[0] > 0) {
                return frogPos[0]--;
            }
        },
        37: function () {
            if (frogPos[1] > 0) {
                return frogPos[1]--;
            }
        },
        39: function () {
            if (frogPos[1] < laneTemplate.length - 1) {
                return frogPos[1]++;
            }
        }
    };

    document.onkeydown = function (_arg) {
        var keyCode;
        keyCode = _arg.keyCode;
        keypressed = keyCode;
        return moves++;
    };

    animate = setInterval(function () {
        var laneCharUnderFrog;
        if (paused) {
            return;
        }
        drawCycles++;
        if (movements[keypressed]) {
            movements[keypressed]();
            keypressed = null;
        }
        if (keypressed === 27) {
            clearInterval(animate);
            return;
        }
        laneTemplate = laneTemplate.substr(-1) + laneTemplate.substr(0, laneTemplate.length - 1);
        street = [start, laneTemplate, laneTemplate.split("").reverse().join(""), laneTemplate.substr(-11) + laneTemplate.substr(0, laneTemplate.length - 11), laneTemplate.split("").reverse().join("").substr(-11) + laneTemplate.split("").reverse().join("").substr(0, laneTemplate.length - 11), goal];
        laneCharUnderFrog = drawFrog(frogPos);
        console.clear();
        console.log("\n\nFROGGER in the browser's console :)\n\nYour Goal: Use the arrow keys to move the '" + frogChar + "' across the street and avoid the cars.\nPress 'Esc' to stop the game.\n\n(Note, if your cursor is in the console you'll need to click on the page outside the console so the arrow keys will work)\n\n\n");
        console.log(street[5]);
        console.log(street[4]);
        console.log(street[3]);
        console.log(street[2]);
        console.log(street[1]);
        console.log(street[0]);

        if (frogPos[0] >= street.length - 1) {
            win();
        }
        if (checkCollision(laneCharUnderFrog)) {
            return lose();
        }
    }, 60);
}
