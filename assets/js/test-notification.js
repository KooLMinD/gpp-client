/*global $, console, readline, cheet, frogger, getCookie, setCookie */
	
$.notifyDefaults({
    delay: 5000,
    mouse_over: 'pause',
    icon_type: 'class',
	spacing: -30,
	placement: {
        from: "top"
    },
    animate: {
        enter: "animated fadeInDown",
        exit: "animated fadeOutUp"
    },
    template: 	
		'<div class="item col-xs-11 col-sm-6 col-md-4 col-lg-3 alert alert-{0}" data-notify="container">' +
		  '<a href="#">' +
			'<div class="panel panel-default">' +
				'<div class="media v-middle">' +
					'<div class="media-left trophy-popup-left" >' +
						'<div class="bronze-trophy rounded">' +
							'<div class="panel-body">' +
							    '<i class="media-object fa-fw fa-2x" data-notify="icon"></i>' +
							'</div>' +
						'</div>' +
					'</div>' +
					'<div class="media-body trophy-popup-right">' +
						'<h4>{1}</h4>' +
						'<p>{2}</p>' +
					'</div>' +
				'</div>' +
			'</div>' +
		  '</a>' +
		'</div>'
});

	function trophyAudio () {
		var trophyPop = new Audio('assets/audio/trophy3.mp3');
		trophyPop.play();
	}

$(document).ready(function () {
    'use strict';
	
    cheet('↑ ↑ ↓ ↓ ← → ← → b a', function () {
        $.notify({
            icon: 'fa fa-history',
            title: 'La storia insegna..',
            message: 'Hai scoperto il Konami code!'
        }, {
            type: 'none',
			onShow: trophyAudio()
        });
    });
    
    cheet('f r o g g e r', function () {
        frogger();
    });


});
